﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Menu.Services
{
    public class TeamHttpService
    {
        static readonly HttpClient client = new HttpClient();

        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "team");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TeamDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<TeamDTO> GetTeamById(int teamId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"team/{teamId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TeamDTO>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<IEnumerable<TeamInfo>> GetSortedTeamsInfo()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "team/SortedTeams");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TeamInfo>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task CreateTeam(TeamDTO teamDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(teamDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(Settings.basePath + "team", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task DeleteTeam(int teamId)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(Settings.basePath + $"team/{teamId}");
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task UpdateTeam(TeamDTO teamDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(teamDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(Settings.basePath + "team", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}
