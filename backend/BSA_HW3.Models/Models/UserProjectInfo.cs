﻿
namespace BSA_HW3.Data.Models
{
    public class UserProjectInfo
    {
        public User User { get; set; }
        public Project LastCreatedProject { get; set; }
        public int AllTasksCount { get; set; }
        public int CancelledOrInProgressTasksCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
