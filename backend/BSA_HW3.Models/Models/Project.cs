﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BSA_HW3.Data.Models
{
    [Table("Project")]
    public class Project
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        public User User { get; set; }

        [Required]
        public int TeamId { get; set; }

        public Team Team { get; set; }

        [Required]
        [StringLength(10)]
        public string ProjectName { get; set; }

        [StringLength(50)]
        public string Descriprion { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime Deadline { get; set; }

        public IEnumerable<Task> Tasks { get; set; }
    }
}
