﻿using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Data.Interfaces
{
    public interface ITeamRepository
    {
        Task<IEnumerable<Team>> GetTeams();
        Task<Team> GetTeamById(int teamId);
        Task CreateTeam(Team team);
        Task DeleteTeam(int teamId);
        Task UpdateTeam(Team team);
    }
}
