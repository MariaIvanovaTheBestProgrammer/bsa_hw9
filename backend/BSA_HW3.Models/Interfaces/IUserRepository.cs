﻿using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Data.Interfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUserById(int userId);
        Task CreateUser(User user);
        Task DeleteUser(int userId);
        Task UpdateUser(User user);
    }
}
