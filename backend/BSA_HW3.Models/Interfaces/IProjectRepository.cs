﻿using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Data.Interfaces
{
    public interface IProjectRepository
    {
        Task<IEnumerable<Project>> GetProjects();
        Task<Project> GetProjectById(int projectId);
        Task CreateProject(Project project);
        Task DeleteProject(int projectId);
        Task UpdateProject(Project project);
    }
}
