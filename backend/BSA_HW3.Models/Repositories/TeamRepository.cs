﻿using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Data.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly DatabaseContext _context;

        public TeamRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task CreateTeam(Team team)
        {
            await _context.AddAsync(team);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteTeam(int teamId)
        {
            var entity = await _context.Teams.Where(e => e.Id == teamId).FirstOrDefaultAsync();
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }


        public async Task<Team> GetTeamById(int teamId)
        {
            return await _context.Teams.Where(e => e.Id == teamId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task UpdateTeam(Team team)
        {
            var tmp = await _context.Teams.FirstOrDefaultAsync(e => e.Id == team.Id);
            if (tmp != null)
            {
                tmp.TeamName = team.TeamName;
                tmp.CreatedAt = team.CreatedAt;
            }
            await _context.SaveChangesAsync();
        }

    }
}
