﻿using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DatabaseContext _context;

        public UserRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task CreateUser(User user)
        {
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUser(int userId)
        {
            var entity = await _context.Users.Where(e => e.Id == userId).FirstOrDefaultAsync();
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetUserById(int userId)
        {
            return await _context.Users.Where(e => e.Id == userId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task UpdateUser(User user)
        {
            var tmp = await _context.Users.FirstOrDefaultAsync(e => e.Id == user.Id);
            if (tmp != null)
            {
                tmp.FirstName = user.FirstName;
                tmp.LastName = user.LastName;
                tmp.TeamId = user.TeamId;
                tmp.Team = user.Team;
                tmp.Email = user.Email;
                tmp.BirthDay = user.BirthDay;
            }
            await _context.SaveChangesAsync();
        }
    }
}
