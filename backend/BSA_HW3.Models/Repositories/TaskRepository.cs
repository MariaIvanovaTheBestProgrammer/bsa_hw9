﻿using BSA_HW3.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BSA_HW3.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace BSA_HW3.Data.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly DatabaseContext _context;

        public TaskRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async System.Threading.Tasks.Task CreateTask(Task task)
        {
            await _context.AddAsync(task);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteTask(int taskId)
        {
            var entity = await _context.Tasks.Where(p => p.TaskId == taskId).FirstOrDefaultAsync();
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<Task> GetTaskById(int taskId)
        {
            return await _context.Tasks.Where(e => e.TaskId == taskId).FirstOrDefaultAsync();
        }

        public async System.Threading.Tasks.Task<IEnumerable<Task>> GetTasks()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async System.Threading.Tasks.Task UpdateTask(Task task)
        {
            var tmp = await _context.Tasks.FirstOrDefaultAsync(e => e.TaskId == task.TaskId);
            if (tmp != null)
            {
                tmp.TaskName = task.TaskName;
                tmp.PerformerId = task.PerformerId;
                tmp.Performer = task.Performer;
                tmp.ProjectId = task.ProjectId;
                tmp.Project = task.Project;
                tmp.TaskState = task.TaskState;
                tmp.CreatedAt = task.CreatedAt;
                tmp.FinishedAt = task.FinishedAt;
                tmp.Description = task.Description;
            }
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task MarkTaskAsDone(int taskId)
        {
            var task = await _context.Tasks.Where(e => e.TaskId == taskId).FirstOrDefaultAsync();
            if(task != null)
            {
                task.TaskState = TaskState.Done;
            }
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<Task> GetRandomTask()
        {
            var random = new Random();
            var listId = _context.Tasks.Select(t => t.TaskName).ToList();
            var taskName = listId[random.Next(listId.Count)];
            return await _context.Tasks.Where(e => e.TaskName == taskName).FirstOrDefaultAsync();
        }
    }
}
