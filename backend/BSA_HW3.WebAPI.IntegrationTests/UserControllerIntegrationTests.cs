﻿using BSA_HW3.Common;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BSA_HW3.WebAPI.IntegrationTests
{
    public class UserControllerIntegrationTests :
        IClassFixture<CustomWebApplicationFactory<Startup>>,
        IDisposable
    {
        private readonly HttpClient _client;

        public UserControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        public void Dispose()
        {
        }

        private async Task<int> AddUser(int id)
        {
            var userDTO = new UserDTO
            {
                Id = id,
                FirstName = "test",
                LastName = "test",
                TeamId = 0,
                RegisteredAt = DateTime.Now,
                BirthDay = new DateTime(2000, 01, 01),
                Email = "test@email.com"
            };
            string jsonInString = JsonConvert.SerializeObject(userDTO);
            var httpResponse = await _client.PostAsync("api/user",
                new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);

            return createdUser.Id;
        }

        [Fact]
        public async Task DeleteUser_ThanResponseCode204AndGetByIdResponseCode404()
        {
            var id = await AddUser(0);
            var httpResponse = await _client.DeleteAsync($"api/user/{id}");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/user/{id}");
            Assert.Equal(HttpStatusCode.NoContent, httpResponseGetById.StatusCode);
        }
    }
}
