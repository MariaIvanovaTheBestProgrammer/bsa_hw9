﻿using BSA_HW3.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BSA_HW3.WebAPI.IntegrationTests
{
    public class TeamControllerIntegrationTests :
        IClassFixture<CustomWebApplicationFactory<Startup>>,
        IDisposable
    {
        private readonly HttpClient _client;

        public TeamControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        public void Dispose()
        {
        }
        [Fact]
        public async Task AddTeam_ThanResponseWithCode201AndCorrespondedBody()
        {
            var team = new TeamDTO
            {
                Id = 0,
                Name = "testteam"
            };
            string jsonInString = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync("api/team",
                new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(team.Name, createdTeam.Name);
        }
    }
}
