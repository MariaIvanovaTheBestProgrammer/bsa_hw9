﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            return Ok(await _userService.GetUsers());
        }
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserDTO>> GetUserById(int userId)
        {
            try
            {
                return Ok(await _userService.GetUserById(userId));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            await _userService.DeleteUser(userId);
            return NoContent();
        }
        [HttpPost]
        public async Task<IActionResult> CreateUser(UserDTO userDTO)
        {
            return StatusCode(201, await _userService.CreateUser(userDTO));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateUser(UserDTO userDTO)
        {
            await _userService.UpdateUser(userDTO);
            return Ok(await _userService.GetUserById(userDTO.Id));
        }
        [HttpGet("SortedTasks")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersSortedByName()
        {
            return Ok(await _userService.GetUsersSortedByName());
        }
        [HttpGet("UserProjectInfo/{userId}")]
        public async Task<ActionResult<UserProjectInfo>> GetUserProjectInfo(int userId)
        {
            return Ok(await _userService.GetUserProjectInfo(userId));
        }
    }
}
