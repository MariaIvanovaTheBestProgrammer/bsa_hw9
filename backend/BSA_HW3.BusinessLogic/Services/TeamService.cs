﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.BusinessLogic.Services
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _iMapper;

        public TeamService(ITeamRepository teamRepository, IUserRepository userRepository, IMapper iMapper)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _iMapper = iMapper;
        }

        public async Task<TeamDTO> CreateTeam(TeamDTO teamDTO)
        {
            var entity = _iMapper.Map<TeamDTO, Team>(teamDTO);
            await _teamRepository.CreateTeam(entity);
            return _iMapper.Map<Team, TeamDTO>(entity);
        }

        public async Task DeleteTeam(int teamId)
        {
            await _teamRepository.DeleteTeam(teamId);
        }

        public async Task<TeamDTO> GetTeamById(int teamId)
        {
            return _iMapper.Map<TeamDTO>(await _teamRepository.GetTeamById(teamId));
        }

        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            var teams = await _teamRepository.GetTeams();
            return teams.Select(e => _iMapper.Map<TeamDTO>(e));
        }

        public async Task UpdateTeam(TeamDTO teamDTO)
        {
            await _teamRepository.UpdateTeam(_iMapper.Map<TeamDTO, Team>(teamDTO));
        }

        //4 linq
        public async Task<IEnumerable<TeamInfo>> GetSortedTeamsInfo()
        {
            var users = await _userRepository.GetUsers();
            var teams = await _teamRepository.GetTeams();
            teams = teams.GroupJoin(
                                    users,
                                    t => t.Id,
                                    u => u.TeamId,
                                    (t, u) =>
                                    {
                                        t.Users = u;
                                        return t;
                                    }
                                    );
            return teams.Where(t => t.Users.All(u => DateTime.Now.Year - u.BirthDay.Year >= 10))
                .Select(t => new TeamInfo
                {
                    Id = t.Id,
                    Name = t.TeamName,
                    Users = t.Users.OrderByDescending(u => u.RegisteredAt)
                });
        }
    }
}
