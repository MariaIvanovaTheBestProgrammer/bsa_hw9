﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.BusinessLogic.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _iMapper;

        public TaskService(ITaskRepository taskRepository, IMapper iMapper)
        {
            _taskRepository = taskRepository;
            _iMapper = iMapper;
        }

        public async System.Threading.Tasks.Task CreateTask(TaskDTO taskDTO)
        {
            var entity = _iMapper.Map<TaskDTO, Task>(taskDTO);
            await _taskRepository.CreateTask(entity);
        }

        public async System.Threading.Tasks.Task DeleteTask(int taskId)
        {
            await _taskRepository.DeleteTask(taskId);
        }

        public async System.Threading.Tasks.Task<TaskDTO> GetTaskById(int taskId)
        {
            return _iMapper.Map<TaskDTO>(await _taskRepository.GetTaskById(taskId));
        }

        public async System.Threading.Tasks.Task<IEnumerable<TaskDTO>> GetTasks()
        {
            var tasks = await _taskRepository.GetTasks();
            return tasks.Select(e => _iMapper.Map<TaskDTO>(e));
        }

        public async System.Threading.Tasks.Task UpdateTask(TaskDTO taskDTO)
        {
            await _taskRepository.UpdateTask(_iMapper.Map<TaskDTO, Task>(taskDTO));
        }

        public async System.Threading.Tasks.Task MarkTaskAsDone(int taskId)
        {
            await _taskRepository.MarkTaskAsDone(taskId);
        }

        public async System.Threading.Tasks.Task<TaskDTO> GetRandomTask()
        {
            return _iMapper.Map<TaskDTO>(await _taskRepository.GetRandomTask());
        }

            //2 linq
        public async System.Threading.Tasks.Task<IEnumerable<TaskDTO>> GetUserTasks(int userId)
        {
            var tasks = await _taskRepository.GetTasks();
            return tasks
                .Where(t => t.PerformerId == userId && t.TaskName.Length < 45)
                .Select(e => _iMapper.Map<TaskDTO>(e));
        }

        //3 linq
        public async System.Threading.Tasks.Task<IEnumerable<TaskInfo>> GetFinishedTaskIdName(int userId)
        {
            var tasks = await _taskRepository.GetTasks();
            return tasks
                .Where(t => t.PerformerId == userId)
                .Where(t => t.TaskState == TaskState.Done && t.FinishedAt?.Year == 2021)
                .Select(t => new TaskInfo { Id = t.TaskId, Name = t.TaskName });
        }
    }
}
