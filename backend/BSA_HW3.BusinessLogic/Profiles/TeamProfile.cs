﻿using AutoMapper;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_HW3.BusinessLogic.Profiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.TeamName));
            CreateMap<TeamDTO, Team>()
                .ForMember(dest => dest.Users, opt => opt.Ignore())
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Name));
        }
    }
}
