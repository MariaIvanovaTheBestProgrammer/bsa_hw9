﻿using BSA_HW3.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetProjects();
        Task<ProjectDTO> GetProjectById(int projectId);
        Task<ProjectDTO> CreateProject(ProjectDTO projectDTO);
        Task DeleteProject(int projectId);
        Task UpdateProject(ProjectDTO projectDTO);
        Task<IEnumerable<KeyValuePair<ProjectDTO, int>>> GetProjectToTasksCountDictionary(int authorId);
        Task<IEnumerable<ProjectInfo>> GetProjectInfo();
    }
}
