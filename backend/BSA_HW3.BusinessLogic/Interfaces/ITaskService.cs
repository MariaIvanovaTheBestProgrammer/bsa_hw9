﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetTasks();
        Task<TaskDTO> GetTaskById(int taskId);
        System.Threading.Tasks.Task CreateTask(TaskDTO taskDTO);
        System.Threading.Tasks.Task DeleteTask(int taskId);
        System.Threading.Tasks.Task UpdateTask(TaskDTO taskDTO);
        Task<IEnumerable<TaskDTO>> GetUserTasks(int userId);
        Task<IEnumerable<TaskInfo>> GetFinishedTaskIdName(int userId);
        System.Threading.Tasks.Task MarkTaskAsDone(int taskId);
        Task<TaskDTO> GetRandomTask();
    }
}
