﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetTeams();
        Task<TeamDTO> GetTeamById(int teamId);
        Task<TeamDTO> CreateTeam(TeamDTO teamDTO);
        Task DeleteTeam(int teamId);
        Task UpdateTeam(TeamDTO teamDTO);
        Task<IEnumerable<TeamInfo>> GetSortedTeamsInfo();
    }
}
