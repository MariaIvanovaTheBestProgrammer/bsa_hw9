﻿using System;
using BSA_HW3.Data.Models;
 
namespace BSA_HW3.Common
{
    public class TaskDTO
    {
        public int TaskId { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState TaskState { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
