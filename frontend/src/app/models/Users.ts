export interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    birthDay: Date;
    registeredAt: Date;
    teamId?: number;
}